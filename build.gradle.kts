import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.list
import org.eclipse.jgit.util.Base64
import org.openstreetmap.josm.gradle.plugin.logSkippedTasks
import org.openstreetmap.josm.gradle.plugin.logTaskDuration
import org.openstreetmap.josm.gradle.plugin.task.GeneratePluginList
import java.io.IOException
import java.net.URL
import java.util.jar.Manifest
import java.util.zip.ZipEntry
import java.util.zip.ZipFile
import java.util.zip.ZipInputStream
import javax.net.ssl.HttpsURLConnection

buildscript {
  repositories {
    gradlePluginPortal() {
      content {
        includeModule("org.openstreetmap.josm", "gradle-josm-plugin")
      }
    }
    jcenter()
  }
  dependencies {
    classpath("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.13.0")
    classpath("org.openstreetmap.josm:gradle-josm-plugin:0.6.5")
  }
}

repositories {
  jcenter()
}

val srcMainDir = File(projectDir, "src/main")
/**
 * The content of this directory will be published to the Gitlab pages site
 */
val outPublicDir = File(buildDir, "public")
val pluginListFile = File(buildDir, "josm-plugins.json")
val josmPluginManifestsFile = File(buildDir, "josm-plugin-manifests.json")

/**
 * Queries the Gitlab API for all projects in the `JOSM/plugin` group, saves them in file [pluginListFile].
 */
val getPluginNames = tasks.create("getPluginNames", DefaultTask::class) {
  outputs.file(pluginListFile)
  outputs.upToDateWhen { false }
  actions = listOf(Action {
    val id = 3712532
    pluginListFile.writeText(
      Json.indented
        .stringify(
          JosmPlugin.serializer().list,
          Json.nonstrict
            .parse(GitlabProject.serializer().list, URL("https://gitlab.com/api/v4/groups/$id/projects").readText())
            .map { JosmPlugin(it.name) }
            .sortedBy { it.name }
            .also {
              require(it.isNotEmpty()) { "No projects found in Gitlab.com group $id" }
              logger.lifecycle("There are ${it.size} projects in GitLab.com group $id: ${it.map { it.name }.joinToString()}")
            }
        )
    )
  })
}

/**
 * Copies the Bootstrap CSS to the `style` directory inside [outPublicDir]
 */
val processCss = tasks.create("processCss", DefaultTask::class) {
  val outputFile = File(outPublicDir, "style/bootstrap.min.css")
  val bootstrapVersion = "4.3.1"

  inputs.property("version", bootstrapVersion)
  outputs.file(outputFile)

  actions = listOf(Action {
    outputFile.writeText(
      configurations.detachedConfiguration(dependencies.create("org.webjars:bootstrap:$bootstrapVersion")).files
        .map { ZipFile(it) }
        .mapNotNull { zipFile -> zipFile.getEntry("META-INF/resources/webjars/bootstrap/$bootstrapVersion/css/${outputFile.name}")?.let { zipFile.getInputStream(it) } }
        .first()
        .bufferedReader()
        .readText()
    )
    logger.lifecycle("""
      |Copy CSS files
      |  from Bootstrap $bootstrapVersion
      |  into ${outputFile.absolutePath}
    """.trimMargin())
  })
}

/**
 * Generates the plugin list in a format that can be used as an update site for JOSM
 */
val buildUpdateSite = tasks.create("buildUpdateSite", GeneratePluginList::class) {
  dependsOn(getPluginNames)
  outputs.upToDateWhen { false } // Always run, so changes in remote URLs are reflected

  outputFile = File(outPublicDir, "plugin-update-site")
  versionSuffix = { "" }

  doFirst {
    val pluginManifests = mutableMapOf<String, Map<String, String>>()

    Json.indented.parse(JosmPlugin.serializer().list, pluginListFile.readText()).parallelStream().forEach {
      logger.lifecycle("Download MANIFEST.MF for ${it.name}…")
      val downloadUrl = URL("https://josm.gitlab.io/plugin/${it.name}/dist/latest/${it.name}.jar")
      val manifestAtts: MutableMap<String, String> = mutableMapOf()
      try {
        val connection = downloadUrl.openConnection() as HttpsURLConnection
        connection.connect()
        if (connection.responseCode != HttpsURLConnection.HTTP_OK || (connection.headerFields["Content-Type"]?.any { it.contains("text/html") } == true)) {
          throw IOException("Response code ${connection.responseCode} with content type ${connection.headerFields["Content-Type"]} for URL $downloadUrl!")
        }
        connection.inputStream.use { inputStream ->
          ZipInputStream(inputStream).use { manifestStream ->
            var entry: ZipEntry? = null
            while (manifestAtts.isEmpty() && manifestStream.nextEntry.also { entry = it } != null) {
              if (entry?.name == "META-INF/MANIFEST.MF") {
                val atts = Manifest(manifestStream).mainAttributes
                manifestAtts.putAll(atts.keys.map { it.toString() to atts.getValue(it).toString() }.sortedBy { it.first }.toMap())
              }
            }
          }
        }
        addPlugin(it.name + ".jar", manifestAtts, downloadUrl)
        pluginManifests.put(it.name, manifestAtts)
        iconBase64Provider = { string -> base64Icon(it.name, string) }
      } catch (e: Exception) {
        logger.error("Failed to read manifest of plugin ${it.name}!", e)
      }
    }

    josmPluginManifestsFile.writeText(Json.indented.stringify(JosmPluginManifests.serializer(), JosmPluginManifests(pluginManifests)))
  }
}

fun base64Icon(pluginName: String, imagePath: String?) = try {
  imagePath?.let {
    URL("https://gitlab.com/JOSM/plugin/$pluginName/raw/master/src/main/resources/$imagePath?inline=false").openConnection().let {
      it.connect()
      "data:${it.contentType};base64,${Base64.encodeBytes(it.inputStream.readBytes())}"
    }
  }
} catch (e: IOException) {
  null
}

val buildIndex = tasks.create("buildIndex", DefaultTask::class) {
  dependsOn(processCss, buildUpdateSite, getPluginNames)

  val templatesDir = File(srcMainDir, "template")
  val outputFile = File(outPublicDir, "index.html")

  inputs.file(josmPluginManifestsFile)
  inputs.dir(templatesDir)
  inputs.file(pluginListFile)
  outputs.files(outputFile)

  actions = listOf(Action {
    val indexTemplate = File(templatesDir, "index.template.html").readText()
    val tableRowTemplate = File(templatesDir, "index/table_row.template.html").readText()
    val errorTableRowTemplate = File(templatesDir, "index/error_table_row.template.html").readText()
    val plugins = Json.indented.parse(JosmPlugin.serializer().list, pluginListFile.readText())

    val pluginManifests = Json(JsonConfiguration.Stable).parse(JosmPluginManifests.serializer(), josmPluginManifestsFile.readText())

    logger.lifecycle("""
      |Create index page
      |  from $pluginListFile
      |  into ${outputFile.absolutePath}
      |         ${plugins.map { "* " + it.name + if (pluginManifests.pluginManifests.containsKey(it.name)) "" else " (error)"  }.joinToString("\n         ") }
      |""".trimMargin()
    )

    outputFile.writeText(
      indexTemplate
        .replace(
          "{{{table_rows}}}",
          plugins
            .partition { pluginManifests.pluginManifests.keys.contains(it.name) } // move failed plugins to the top
            .let { it.second.sortedBy { it.name }.plus(it.first.sortedBy { it.name })}
            .map {
              val manifestAtts = pluginManifests.pluginManifests.get(it.name)
              if (manifestAtts == null) {
                errorTableRowTemplate
                  .replace("{{{plugin_name}}}", it.name)
              } else {
                tableRowTemplate
                  .replace("{{{plugin_name}}}", it.name)
                  .replace("{{{plugin_icon}}}", base64Icon(it.name, manifestAtts["Plugin-Icon"])?.let { """<img src="$it" style="max-width:1.5em;max-height:1.5em">""" } ?: "")
                  .replace("{{{plugin_author}}}", manifestAtts["Author"]?.replace(Regex("<[^>@]+@[^>]+>"), "<…@…>") ?: "—")
                  .replace("{{{plugin_version}}}", manifestAtts["Plugin-Version"] ?: """<span class="text-muted">???</span>""")
              }
            }
            .joinToString(""))
        .also {
          val match = Regex("\\{\\{\\{[a-zA-Z_-]+\\}\\}\\}").find(it)
          require(match == null) {
            "Not all template variables were replaced! Result would still contain ${match?.groupValues?.first()}."
          }
        }
    )
  })
}

tasks.create("clean", Delete::class) {
  delete(buildDir)
}

tasks.create("build") {
  dependsOn(buildIndex)
}

logSkippedTasks()
gradle.taskGraph.logTaskDuration()
