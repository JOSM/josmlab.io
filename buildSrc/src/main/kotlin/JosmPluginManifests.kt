import kotlinx.serialization.Serializable
import org.gradle.internal.impldep.com.fasterxml.jackson.annotation.JsonUnwrapped

@Serializable
data class JosmPluginManifests(
  val pluginManifests: Map<String, Map<String, String>>
)