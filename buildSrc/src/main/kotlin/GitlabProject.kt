import kotlinx.serialization.Serializable

@Serializable
data class GitlabProject(val name: String)