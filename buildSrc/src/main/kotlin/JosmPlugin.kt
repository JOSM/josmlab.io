import kotlinx.serialization.Serializable
import java.io.IOException
import java.net.URL

@Serializable
data class JosmPlugin(val name: String)