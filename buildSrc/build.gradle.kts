plugins {
  kotlin("jvm").version("1.3.50")
  kotlin("plugin.serialization").version("1.3.50")
}

repositories {
  jcenter()
}
dependencies {
  implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.13.0")
}